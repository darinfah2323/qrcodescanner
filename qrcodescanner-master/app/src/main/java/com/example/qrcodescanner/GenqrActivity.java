package com.example.qrcodescanner;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class GenqrActivity extends AppCompatActivity {
    Button generate_QRCode;
    ImageView qrCode;
    EditText mEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genqr);

        generate_QRCode=(Button)findViewById(R.id.generate_qr);
        qrCode=(ImageView)findViewById(R.id.imageView);
        mEditText=(EditText)findViewById(R.id.editText);
        generate_QRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text=mEditText.getText().toString();
                if(TextUtils.isEmpty(text)){
                    Toast.makeText(getApplicationContext(), "Please Input your text.",
                            Toast.LENGTH_SHORT).show();
                }else {
                    QRCodeWriter writer = new QRCodeWriter();
                    try {
                        BitMatrix bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, 512, 512);
                        int width = bitMatrix.getWidth();
                        int height = bitMatrix.getHeight();
                        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                        for (int x = 0; x < width; x++) {
                            for (int y = 0; y < height; y++) {
                                bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                            }
                        }
                        if (bmp != null) {
                            qrCode.setImageBitmap(bmp);
                        } else {
                            Toast.makeText(getApplicationContext(), "User Input Error",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } catch (WriterException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }
}